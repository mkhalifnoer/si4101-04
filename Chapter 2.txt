System Requirement

1. Bahasa pemograman yang dipakai adalah java untuk android dan swift untuk IOS dan WEB application.
2. Versioning control yang dipakai adalah Git.
3. Aplikasi berbasis mobile (Android / IOS)
4. Web server Nginx
5. REST API menggunakan node js
6. Database dengan ORACLE.
7. Server menggunakan DELL PowerEdge R940 10 buah.
8. Sistem Operasi menggunakan Ubuntu.
9. Versi OS yang bisa menggunakan aplikasi yaitu android versi 6.0 keatas dan versi IOS 9.0 keatas. 